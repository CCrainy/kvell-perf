#!/bin/bash
scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
mainDir="${scriptDir}/.."
tcmalloc="env LD_PRELOAD=${HOME}/gperftools/.libs/libtcmalloc.so "

#/!\ This script will NOT work if you modify main.c

#
#Run YCSB A B C (D = A, F =B)
#4 workers per disk, 4 load injectors
#
rm -rf /scratch*/kvell/*
#cp -r /data/kvell0/* /scratch0/kvell/
#cp -r /data/kvell1/* /scratch1/kvell/
echo "Run YCSB A"
git checkout -- ${mainDir}/main.c
cat ${mainDir}/main.c | perl -pe 's://.nb_load_injectors = 4:.nb_load_injectors = 4:' | perl -pe 's:[^/]ycsb_b_zipfian,: //ycsb_b_zipfian,:' | perl -pe 's://ycsb_a_zipfian,:ycsb_a_zipfian,:' > ${mainDir}/main.c.tmp
mv ${mainDir}/main.c.tmp ${mainDir}/main.c
make -C ${mainDir} -j
${tcmalloc} ${mainDir}/main 2 4 | tee log_ycsb_$1_a$2

cp ${mainDir}/main.c ${mainDir}/main.c.a


rm -rf /scratch*/kvell/*
#cp -r /data/kvell0/* /scratch0/kvell/
#cp -r /data/kvell1/* /scratch1/kvell/
echo "Run YCSB B"
git checkout -- ${mainDir}/main.c
cat ${mainDir}/main.c | perl -pe 's://.nb_load_injectors = 4:.nb_load_injectors = 4:' | perl -pe 's:[^/]ycsb_a_zipfian,: //ycsb_a_zipfian,:' | perl -pe 's://ycsb_b_zipfian,:ycsb_b_zipfian,:' > ${mainDir}/main.c.tmp
mv ${mainDir}/main.c.tmp ${mainDir}/main.c
make -C ${mainDir} -j
${tcmalloc} ${mainDir}/main 2 4 | tee log_ycsb_$1_b$2

cp ${mainDir}/main.c ${mainDir}/main.c.b